# repository

The infrastucture team have built a mechanism to simplify the repository maintenance.
The repositories and repository members about openEuler are addressed in [openeuler.yaml](openeuler.yaml),
meanwhile the repositories and repository members about src-openEuler are addressed in [src-openeuler.yaml](src-openeuler.yaml).

If you want to ```create a repository```, ```add members for a repository```
and ```remove members from a repository```,
please following [the instructions](https://gitee.com/openeuler/community/blob/master/en/Repository.md) for details.
